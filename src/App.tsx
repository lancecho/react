// main application file
import  * as React from "react";
import {render} from "react-dom";

// use this option or the other one underneath
// const App = (props: {title: string}) => <h1>{props.title}</h1>;
// render(<App title="Hello from React TypeScript"/>, document.getElementById("root"));

const App =()=> <h1>Hello from React TypeScript</h1>;
render(<App />, document.getElementById("root"));
