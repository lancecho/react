// start register hook for tyupescript
require('ts-node').register({
    // use this tsnode config instead of the other one
    project: './tsconfig.tsnode.json'
});

module.exports = require('./config/webpack.config').default;